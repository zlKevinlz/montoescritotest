package prueba.veritran.net.pruebauniversidad;

import java.util.HashMap;
import java.util.Map;

public class MontoEscrito {
    private static Map<Integer,String> parseoNumeros = new HashMap<Integer,String>();
    public static void main(String[] args) { MontoEscrito.getMontoEscrito(0);}

    public static String getMontoEscrito(Integer valor){
        String resultado = "";
        crearParseNumeros();

        if(parseoNumeros.containsKey(valor)) {
            return parseoNumeros.get(valor);
        }
        else System.out.print(decenas(3290641));
        return resultado;
    }

    public static String decenas(int x){
        String resultado = "";
        if(x <= 15 || x == 20 || x == 30 || x == 40 || x == 50 || x == 60 || x == 70 || x == 80 || x == 90){
            return resultado = getMontoEscrito(x);
        }
        if(x > 15 && x < 100){
            if ((x > 15) && (x < 20)) {
                resultado = "dieci" + getMontoEscrito(x % 10);
            }
            if ((x > 20) && (x < 30)) {
                resultado = "veinti" + getMontoEscrito(x % 10);
            }
            if ((x > 30) && (x < 40)) {
                resultado = "treinta y " + getMontoEscrito(x % 10);
            }
            if ((x > 40) && (x < 50)) {
                resultado = "cuarenta y " + getMontoEscrito(x % 10);
            }
            if ((x > 50) && (x < 60)) {
                resultado = "cincuenta y " + getMontoEscrito(x % 10);
            }
            if ((x > 60) && (x < 70)) {
                resultado = "sesenta y " + getMontoEscrito(x % 10);
            }
            if ((x > 70) && (x < 80)) {
                resultado = "setenta y " + getMontoEscrito(x % 10);
            }
            if ((x > 80) && (x < 90)) {
                resultado = "ochenta y " + getMontoEscrito(x % 10);
            }
            if ((x > 90) && (x < 100)) {
                resultado = "noventa y " + getMontoEscrito(x % 10);
            }
        } else {
            resultado = centena(x);
        }
        return resultado;
    }

    public static String centena(int x) {
        String resultado = "";

        switch (x) {
            case 200:
                resultado = "doscientos";
                break;
            case 300:
                resultado = "trescientos";
                break;
            case 400:
                resultado = "cuatrocientos";
                break;
            case 600:
                resultado = "seiscientos";
                break;
            case 800:
                resultado = "ochocientos";
                break;
            default:
                if(x > 99 && x < 1000) {
                    if (x == 100 || x == 500 || x == 700 || x == 900) {
                        return resultado = getMontoEscrito(x);
                    }
                    if ((x > 100) && (x < 200)) {
                        resultado = "ciento " + decenas(x % 100);
                    }
                    if (x > 200 && x < 400 || x > 600 && x < 700 || x > 800 && x < 900) {
                        resultado = decenas(x / 100) + "cientos ";
                        resultado += decenas(x % 100);
                    }
                    if (x > 500 && x < 600) {
                        resultado = "quinientos " + decenas(x % 100);
                    }
                    if (x > 700 && x < 800) {
                        resultado = "setecientos " + decenas(x % 100);
                    }
                    if (x > 900 && x < 1000) {
                        resultado = "novecientos " + decenas(x % 100);
                    }
                } else {
                    resultado = mil(x);
                }
        }
        return resultado;
    }

    public static String mil(int x) {
        String resultado = "";
        if(x > 999 && x < 1000000) {
            if (x == 1000) {
                return resultado = getMontoEscrito(x);
            }
            if ((x > 1000) && (x < 2000)) {
                if ((x % 1000) != 000) {
                    resultado = "mil " + decenas(x % 1000);
                }
            }

            if ((x >= 2000) && (x < 1000000)) {
                resultado = decenas(x / 1000) + " mil ";
                if ((x % 1000) != 000) {
                    resultado += decenas(x % 1000);
                }
            }
        } else {
            resultado = millon(x);
        }
        return resultado;
    }

    public static String millon(int n) {
        String resultado = "";
        if (n == 1000000) {
            return resultado = getMontoEscrito(n);
        }
        if (n > 1000000 && n < 2000000) {
            if ((n % 1000000) != 000000) {
                resultado = "un millon " + decenas(n % 1000000);
            }
        }
        if (n >= 2000000 && n <= 999999999) {
            resultado = decenas(n / 1000000) + " millones ";
            if ((n % 1000000) != 000000) {
                resultado += decenas(n % 1000000);
            }
        }
        return resultado;
    }

    private static void crearParseNumeros() {
        parseoNumeros.put(0,"cero");
        parseoNumeros.put(1,"uno");
        parseoNumeros.put(2,"dos");
        parseoNumeros.put(3,"tres");
        parseoNumeros.put(4,"cuatro");
        parseoNumeros.put(5,"cinco");
        parseoNumeros.put(6,"seis");
        parseoNumeros.put(7,"siete");
        parseoNumeros.put(8,"ocho");
        parseoNumeros.put(9,"nueve");
        parseoNumeros.put(10, "diez");
        parseoNumeros.put(11, "once");
        parseoNumeros.put(12, "doce");
        parseoNumeros.put(13, "trece");
        parseoNumeros.put(14, "catorce");
        parseoNumeros.put(15, "quince");
        parseoNumeros.put(20, "veinte");
        parseoNumeros.put(30, "treinta");
        parseoNumeros.put(40, "cuarenta");
        parseoNumeros.put(50, "cincuenta");
        parseoNumeros.put(60, "sesenta");
        parseoNumeros.put(70, "setenta");
        parseoNumeros.put(80, "ochenta");
        parseoNumeros.put(90, "noventa");
        parseoNumeros.put(100, "cien");
        parseoNumeros.put(500, "quinientos");
        parseoNumeros.put(700, "setecientos");
        parseoNumeros.put(900, "novecientos");
        parseoNumeros.put(1000, "mil");
        parseoNumeros.put(1000000, "un millon");
    }
}
